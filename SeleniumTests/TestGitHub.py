import pytest
from PageObjects import GitHub
from Consts import user_name, password


class TestGitHub():

    def setup(self):
        self.gh = GitHub()

    @pytest.mark.parametrize('expected_index, expected_record_value', [(0, 'Explore'), (1, 'Features')])
    def test_not_logged(self, expected_index, expected_record_value):
        records = self.gh.get_panel_records()
        assert unicode(expected_record_value) == records[expected_index]

    @pytest.mark.parametrize('expected_index, expected_record_value', [(0, 'Pull requests'), (1, 'Issues')])
    def test_logged(self, expected_index, expected_record_value):
        self.gh.click_sign_in()
        self.gh.login(user_name, password)
        records = self.gh.get_panel_records()
        assert unicode(expected_record_value) == records[expected_index]

    def teardown(self):
        self.gh.quit()


if __name__=='__main__':
    pytest.main('%s' % __file__)