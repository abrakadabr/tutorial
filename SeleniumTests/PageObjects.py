from selenium import webdriver

class GitHub():

    def __init__(self):
        self.driver = webdriver.Firefox()
        self.driver.get('https://github.com')

    def get_panel_records(self):
        elements = self.driver.find_elements_by_xpath('.//*[@class="header-nav left"]/li')
        return [element.text for element in elements]

    def click_sign_in(self):
        self.driver.find_element_by_xpath('.//*[@class="header-actions"]/a[2]').click()

    def login(self, login_name, password):
        self.driver.find_element_by_id('login_field').send_keys(login_name)
        self.driver.find_element_by_id('password').send_keys(password)
        self.driver.find_element_by_xpath(".//*[@id='login']/form/div[3]/input[3]").click()

    def quit(self):
        self.driver.quit()

if __name__ == '__main__':
    gh = GitHub()
    print gh.get_panel_records()
    gh.click_sign_in()
    gh.login('Abrakadabra1', 'Abrakadabra123')
    print gh.get_panel_records()



