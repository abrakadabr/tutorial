import json
import os


sample_json_path = os.path.join(os.path.dirname(__file__), 'Json/sample_json.json')
sample_json = open(sample_json_path).read()
json_list = json.loads(sample_json)

for element in json_list:
    print '%s/%s' % (element['productId'], element['productName'])