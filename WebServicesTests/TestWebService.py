import pytest
import requests
import json

from Consts import url


class TestWebService(object):

    @pytest.mark.parametrize('expected_code', [200])
    def test_response_status(self, expected_code):
        get_response = requests.get(url)
        assert get_response.status_code == expected_code, 'Expected %s response' % str(expected_code) # assert get_response.ok

    @pytest.mark.parametrize('product_id', ['1', '2', '3'])
    def test_element_is_present(self, product_id):
        get_response = requests.get(url)
        json_response = json.loads(get_response.text)

        for element in json_response:
            if product_id == element['productId']:
                break
        else:
            assert False, 'Response does not contain productId = %s' % product_id


    @pytest.mark.parametrize('product_id, product_name', [
                                                            ('1', 'test1'),
                                                            ('2', 'test2'),
                                                            ('3', 'test3')
                                                          ])
    def test_element_values(self, product_id, product_name):
        get_response = requests.get(url)
        json_response = json.loads(get_response.text)

        for element in json_response:
            if product_id == element['productId']:
                assert product_name == element['productName']

    @pytest.mark.parametrize('number_of_records', [3])
    def test_number_of_records(self, number_of_records):
        get_response = requests.get(url)
        json_response = json.loads(get_response.text)

        assert len(json_response) == number_of_records, 'Expected %s records' % str(number_of_records)



if __name__=='__main__':
    pytest.main('%s' % __file__)